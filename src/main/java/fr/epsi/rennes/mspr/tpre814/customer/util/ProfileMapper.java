package fr.epsi.rennes.mspr.tpre814.customer.util;

import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.UserProfileDTO;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.UserProfileEntity;
import fr.epsi.rennes.mspr.tpre814.shared.models.UserProfile;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public class ProfileMapper {

    public static UserProfileDTO toDTO(UserProfileEntity userProfileDTO) {
        if (userProfileDTO == null || userProfileDTO.getId() == null) {
            return null;
        }
        return new UserProfileDTO(
                userProfileDTO.getId().toString(),
                userProfileDTO.getFirstName(),
                userProfileDTO.getLastName()
        );
    }

    public static UserProfile toEntity(UserProfileDTO userProfileDTO) {
        return new UserProfile(
                UUID.fromString(userProfileDTO.id()),
                userProfileDTO.firstName(),
                userProfileDTO.lastName()
        );
    }

    public static @NotEmpty @NotNull UserProfileEntity toEntity(UserProfile userProfileDTO) {
        if (userProfileDTO == null || userProfileDTO.getId() == null) {
            return null;
        }
        UserProfileEntity userProfile = new UserProfileEntity();
        userProfile.setId(userProfileDTO.getId());
        userProfile.setFirstName(userProfileDTO.getFirstName());
        userProfile.setLastName(userProfileDTO.getLastName());
        return userProfile;
    }

    public static UserProfile toProfile(UserProfileDTO profile) {
        if (profile == null || profile.id() == null) {
            return null;
        }
        UserProfile userProfile = new UserProfile();
        userProfile.setId(UUID.fromString(profile.id()));
        userProfile.setFirstName(profile.firstName());
        userProfile.setLastName(profile.lastName());
        return userProfile;

    }
}
