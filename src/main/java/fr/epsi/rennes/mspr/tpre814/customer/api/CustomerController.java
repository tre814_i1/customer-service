package fr.epsi.rennes.mspr.tpre814.customer.api;

import fr.epsi.rennes.mspr.tpre814.customer.api.interfaces.CRUDInterface;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import fr.epsi.rennes.mspr.tpre814.customer.services.interfaces.CustomerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/api/v2/customer")
@RequiredArgsConstructor
public final class CustomerController implements CRUDInterface<CustomerEntity, UUID, CustomerDTO> {

    private final CustomerService service;

    @Override
    @PostMapping
    public CustomerDTO create(@RequestBody @NonNull CustomerEntity customer) {
        return service.create(customer);
    }


    // TO BE REMOVED (tests before broker only)
    @Override
    @GetMapping(value = {"/all"})
    public List<CustomerDTO> all() {
        return service.all();
    }

    @Override
    @PutMapping
    public CustomerDTO update(@RequestBody @NonNull CustomerEntity entity) {
        return service.update(entity);
    }

    @Override
    @DeleteMapping(value = {"/{customerId}"})
    public void delete(@PathVariable UUID customerId) {
        service.delete(customerId);
    }

    @GetMapping(value = {"/{customerId}"})
    public CustomerDTO get(@PathVariable UUID customerId) {
        return service.get(customerId);
    }
}
