package fr.epsi.rennes.mspr.tpre814.customer.util;

import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CompanyDTO;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CompanyEntity;
import fr.epsi.rennes.mspr.tpre814.shared.models.Company;

import java.util.UUID;

public class CompanyMapper {

    public static CompanyDTO toDTO(Company company) {
        return new CompanyDTO(
                company.getId().toString(),
                company.getName(),
                company.getSiret(),
                AddressMapper.toDTO(company.getAddress())
        );
    }

    public static CompanyDTO toDTO(CompanyEntity company) {
        if (company == null || company.getId() == null) {
            return null;
        }
        return new CompanyDTO(
                company.getId().toString(),
                company.getName(),
                company.getSiret(),
                AddressMapper.toDTO(company.getAddress())
        );
    }

    public static CompanyEntity toEntity(CompanyDTO companyDTO) {
        CompanyEntity company = new CompanyEntity();
        company.setId(UUID.fromString(companyDTO.id()));
        company.setName(companyDTO.name());
        company.setSiret(companyDTO.siret());
        company.setAddress(AddressMapper.toEntity(companyDTO.address()));
        return company;

    }

    public static CompanyEntity toEntity(Company companyDTO) {
        if (companyDTO == null || companyDTO.getId() == null) {
            return null;
        }
        CompanyEntity company = new CompanyEntity();
        company.setId(companyDTO.getId());
        company.setName(companyDTO.getName());
        company.setSiret(companyDTO.getSiret());
        company.setAddress(AddressMapper.toEntity(companyDTO.getAddress()));
        return company;

    }

    public static Company toCompany(CompanyDTO company) {
        if (company == null || company.id() == null) {
            return null;
        }
        Company companyDTO = new Company();
        companyDTO.setId(UUID.fromString(company.id()));
        companyDTO.setName(company.name());
        companyDTO.setSiret(company.siret());
        companyDTO.setAddress(AddressMapper.toAddress(company.address()));
        return companyDTO;
    }
}
