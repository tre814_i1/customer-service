package fr.epsi.rennes.mspr.tpre814.customer.error;

public class CustomerNotFoundException extends RuntimeException {
    public CustomerNotFoundException() {
        super("Customer not found");
    }
}
