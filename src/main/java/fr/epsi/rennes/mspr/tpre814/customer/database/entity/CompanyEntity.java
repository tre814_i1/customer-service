package fr.epsi.rennes.mspr.tpre814.customer.database.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity
public class CompanyEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 8848685385087113457L;

    @Id
    @jakarta.persistence.GeneratedValue(generator = "UUID")
    private UUID id;

    private String name;

    private String siret;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private AddressEntity address;
}
