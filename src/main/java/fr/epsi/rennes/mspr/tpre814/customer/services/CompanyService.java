package fr.epsi.rennes.mspr.tpre814.customer.services;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CompanyEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.CompanyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyService {
    private final  CompanyRepository repo;
    private final  AddressService addrS;

    public CompanyEntity save(CompanyEntity company) {
        if (company == null) {
            log.warn("Company is null");
            return null;
        }
        if (company.getAddress() != null) {
            log.info("Saving new Company Address: {}", company.getAddress());
            company.setAddress(addrS.save(company.getAddress()));
        }
        log.info("Saving company: {}", company);
        return repo.save(company);
    }
}
