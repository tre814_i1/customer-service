package fr.epsi.rennes.mspr.tpre814.customer.util.dtos;

import java.util.UUID;

public record AddressDTO(UUID id, String street, String city, String zipcode, String country) {
}
