package fr.epsi.rennes.mspr.tpre814.customer.util.dtos;

/**
 *     UUID id;
 *     String createdAt;
 *     String firstName;
 *     String lastName;
 * @param id
 * @param firstName
 * @param lastName
 */
public record UserProfileDTO(String id, String firstName, String lastName) {
}