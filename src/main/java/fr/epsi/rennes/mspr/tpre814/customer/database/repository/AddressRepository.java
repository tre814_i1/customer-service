package fr.epsi.rennes.mspr.tpre814.customer.database.repository;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.AddressEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, UUID> {

}
