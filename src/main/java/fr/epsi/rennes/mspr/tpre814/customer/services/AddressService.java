package fr.epsi.rennes.mspr.tpre814.customer.services;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.AddressEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.AddressRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AddressService {
    private final  AddressRepository repo;

    public AddressEntity save(AddressEntity address) {
        if (address == null) {
            log.warn("Address is null");
            return null;
        }
        log.info("Saving address: {}", address);
        return repo.save(address);
    }

    public AddressEntity findById(UUID id) {
        if (id == null) {
            log.warn("Id is null");
            return null;
        }
        log.info("Finding address by id: {}", id);
        return repo.findById(id).orElse(null);
    }
}
