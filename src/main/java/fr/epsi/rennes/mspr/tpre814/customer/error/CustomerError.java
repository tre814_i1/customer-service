package fr.epsi.rennes.mspr.tpre814.customer.error;

public class CustomerError extends RuntimeException {
    public CustomerError(String msg) {
        super(msg);
    }

    public CustomerError(String msg, Throwable cause) {
        super(msg, cause);
    }
}
