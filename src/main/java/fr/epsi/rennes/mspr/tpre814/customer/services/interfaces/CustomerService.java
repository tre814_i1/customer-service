package fr.epsi.rennes.mspr.tpre814.customer.services.interfaces;

import fr.epsi.rennes.mspr.tpre814.customer.api.interfaces.CRUDInterface;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;

import java.util.List;
import java.util.UUID;

public interface CustomerService extends CRUDInterface<CustomerEntity, UUID, CustomerDTO> {
    List<CustomerDTO> createAll(List<CustomerEntity> Customer);

    void handleCustomerOrdered(UUID customerId, UUID orderId);

    List<CustomerEntity> findAllById(List<UUID> list);

    CustomerEntity getEntity(UUID customerId);
}
