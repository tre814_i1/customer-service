package fr.epsi.rennes.mspr.tpre814.customer.database.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity
public class UserProfileEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 8457396466739115897L;
    @Id
    @jakarta.persistence.GeneratedValue(generator = "UUID")
    UUID id;
    String createdAt;
    String firstName;
    String lastName;
}
