package fr.epsi.rennes.mspr.tpre814.customer.services;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.CustomerRepository;
import fr.epsi.rennes.mspr.tpre814.customer.error.CustomerError;
import fr.epsi.rennes.mspr.tpre814.customer.error.CustomerNotFoundException;
import fr.epsi.rennes.mspr.tpre814.customer.services.interfaces.CustomerService;
import fr.epsi.rennes.mspr.tpre814.customer.services.kafka.OrderRequestService;
import fr.epsi.rennes.mspr.tpre814.customer.util.CustomerMapper;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.OrderResponseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;
import fr.epsi.rennes.mspr.tpre814.shared.models.Company;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.UserProfile;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * The main goal of this class is to wait for messages from the given topics to be consumed,
 * in order to execute a process and finally respond with an ACK.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService { // auto plugs on tests with containers
    private final CustomerRepository repository;
    private final AddressService addrS;
    private final CompanyService compS;
    private final ProfileService profS;
    private final OrderRequestService orderRequestService;


    @Deprecated(since = "0.0.8", forRemoval = false)
    public static Customer fakeCustomer(UUID customerId) {
        return new Customer(customerId, "Faker",
                new UserProfile(UUID.randomUUID(), "Bob", "Doe"),  // Fake user profile
                new Address(
                        UUID.randomUUID(),
                        "Faker",
                        "Faker",
                        "Faker",
                        "Faker"
                ),  // Fake delivery address
                new Company(UUID.randomUUID(),
                        "EPSI",
                        "123456789 00001",
                        new Address(
                                UUID.randomUUID(),
                                "EPSI",
                                "EPSI",
                                "EPSI",
                                "EPSI"
                        )
                )
        );  // Fake company address
    }

    @Override
    @Transactional(transactionManager = "transactionManager")
    public CustomerDTO create(@NonNull CustomerEntity entity) {
        log.info("Creating customer: {}", entity);
        try {
            repository.findByUsername(entity.getUsername()).ifPresent(customerEntity -> {
                throw new CustomerError("Customer already exists");
            });
        } catch (CustomerError e) {
            log.error(e.getMessage(), e);
            return null;
        }
        entity.setAddress(addrS.save(entity.getAddress()));
        entity.setCompany(compS.save(entity.getCompany()));
        entity.setProfile(profS.save(entity.getProfile()));
        CustomerDTO result = CustomerMapper.toDTO(repository.save(entity));
        log.info("Customer created: {}", result);
        return result;
    }

    @Override
    public CustomerDTO get(UUID id) {
        log.info("Getting customer with id: {}", id);
        CustomerEntity customer = getEntity(id);

        CustomerDTO result = null;
        if (!customer.getOrders().isEmpty()) {
            CompletableFuture<BaseEvent> orders = orderRequestService.getOrders(id, customer.getOrders());
            orders.join().toJson();
            try {
                result = CustomerMapper.toDTO(customer, (OrderResponseEvent) orders.join());
            } catch (Exception e) {
                log.error("Error getting orders", e);
            }
        } else {
            result = CustomerMapper.toDTO(customer);
        }
        return result == null ? CustomerMapper.toDTO(customer) : result;
    }

    public CustomerEntity getEntity(UUID id) {
        return repository.findById(id).orElseThrow(CustomerNotFoundException::new);
    }

    @Override
    public List<CustomerDTO> all() {
        log.info("Getting all customers...");
        return CustomerMapper.toDTO(repository.findAll());
    }

    @Override
    public CustomerDTO update(CustomerEntity entity) {
        CustomerEntity dbCustomer = getEntity(entity.getId());
        log.info("Updating customer: {}\nwith : {}", dbCustomer, entity);
        CustomerMapper.updateWith(dbCustomer, entity);
        return CustomerMapper.toDTO(repository.save(dbCustomer));
    }

    @Override
    public void delete(UUID id) {
        repository.deleteById(id);
    }

    @Override
    public List<CustomerDTO> createAll(List<CustomerEntity> customer) {
        try {
            return customer.stream().map(this::create).toList();
        } catch (CustomerError e) {
            return List.of();
        }
    }

    @Override
    public void handleCustomerOrdered(UUID customerId, UUID orderId) {
        CustomerEntity entity = getEntity(customerId);
        entity.getOrders().add(orderId);
        repository.save(entity);
    }

    @Override
    public List<CustomerEntity> findAllById(List<UUID> list) {
        return repository.findAllById(list);
    }
}
