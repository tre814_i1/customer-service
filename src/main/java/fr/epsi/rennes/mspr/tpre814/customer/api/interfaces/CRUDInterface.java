package fr.epsi.rennes.mspr.tpre814.customer.api.interfaces;

import java.util.List;

public interface CRUDInterface<T, K, R> {

    R create(T entity);

    R get(K id);

    List<R> all();

    R update(T entity);

    void delete(K id);
}
