package fr.epsi.rennes.mspr.tpre814.customer.database.entity;

import jakarta.persistence.Entity;
import lombok.Data;

@Entity
@Data
public class AddressEntity implements java.io.Serializable {

    @java.io.Serial
    private static final long serialVersionUID = 1L;

    @jakarta.persistence.Id
    @jakarta.persistence.GeneratedValue(generator = "UUID")
    private java.util.UUID id;

    @jakarta.validation.constraints.NotEmpty
    @jakarta.validation.constraints.NotNull
    @jakarta.persistence.Column(nullable = false)
    private String street;

    @jakarta.validation.constraints.NotEmpty
    @jakarta.validation.constraints.NotNull
    @jakarta.persistence.Column(nullable = false)
    private String city;

    @jakarta.validation.constraints.NotEmpty
    @jakarta.validation.constraints.NotNull
    @jakarta.persistence.Column(nullable = false)
    private String postalCode;

    @jakarta.validation.constraints.NotEmpty
    @jakarta.validation.constraints.NotNull
    @jakarta.persistence.Column(nullable = false)
    private String country;
}
