package fr.epsi.rennes.mspr.tpre814.customer.services.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.AbstractEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import jakarta.annotation.PreDestroy;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.naming.ServiceUnavailableException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.*;
import static org.apache.kafka.common.utils.Utils.sleep;

/**
 * Define the base structure and methods to request and await response asynchronously
 * This class is abstract and should be extended by the services that need to await for responses
 */
@Slf4j
public abstract class AwaitTable {

    static final ObjectMapper mapper = new ObjectMapper();
    final ConcurrentHashMap<UUID, CompletableFuture<BaseEvent>> pendingRequests = new ConcurrentHashMap<>();

    @SneakyThrows
    @PreDestroy
    void tearDown() {
        sleep(300);
        log.info("Order Service stopped, awaiting for pending orders");
        pendingRequests.forEach((k, v) ->
                v.completeExceptionally(
                        new ServiceUnavailableException(
                                "Service is shutting down")));
    }

    CompletableFuture<BaseEvent> newRequest(UUID id) {
        CompletableFuture<BaseEvent> fut = new CompletableFuture<>();
        // define timeout on creation
        fut.orTimeout(TIMEOUT, TIMEOUT_UNIT);
        pendingRequests.put(id, fut);
        return fut;
    }


    /**
     * Get the pending request
     *
     * @param id the id of the request
     * @return the request
     * @throws IllegalStateException if the request is not found
     */
    CompletableFuture<BaseEvent> getPending(UUID id) {
        CompletableFuture<BaseEvent> fut = pendingRequests.remove(id);
        if (fut == null) {
            throw new IllegalStateException("No pending request for " + id);
        }
        return fut;
    }

    /**
     * Get the response from the pending requests
     *
     * @param id the id of the request
     * @return the response
     * @throws InterruptedException if the request is not found or if the response is not received in time
     * @throws ExecutionException   if the request is not found or if the response is not received in time
     * @throws TimeoutException     if the request is not found or if the response is not received in time
     * @throws ServiceUnavailableException if the service is shutting down
     */
    @SneakyThrows
    BaseEvent getResponse(UUID id) {
        return getPending(id).get(10, java.util.concurrent.TimeUnit.SECONDS);
    }

}
