package fr.epsi.rennes.mspr.tpre814.customer.database.entity;


import jakarta.persistence.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
public class CustomerEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -1840553357363193935L;
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;

    @jakarta.validation.constraints.NotEmpty
    @jakarta.validation.constraints.NotNull
    @jakarta.persistence.Column(nullable = false)
    private String username;

    @OneToOne(cascade = CascadeType.ALL, optional = true)
    @JoinColumn(name = "profile_id")
    private UserProfileEntity profile = new UserProfileEntity();

    @ManyToOne
    @JoinColumn(name = "address_id")
    private AddressEntity address;
    @ManyToOne
    @JoinColumn(name = "company_id")
    private CompanyEntity company;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "orders", joinColumns = @JoinColumn(name = "customer_id"))
    private List<UUID> orders = new ArrayList<>();
}
