package fr.epsi.rennes.mspr.tpre814.customer.util;

import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.AddressDTO;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.AddressEntity;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;

import java.util.UUID;

public class AddressMapper {

    public static AddressDTO toDTO(Address address) {
        if (address == null || address.getId() == null) {
            return null;
        }
        return new AddressDTO(
                address.getId(),
                address.getStreet(),
                address.getCity(),
                address.getZipcode(),
                address.getCountry());
    }

    public static AddressDTO toDTO(AddressEntity address) {
        if (address == null || address.getId() == null) {
            return new AddressDTO(UUID.randomUUID(), null, null, null, null);
        }
        return new AddressDTO(
                address.getId(),
                address.getStreet(),
                address.getCity(),
                address.getPostalCode(),
                address.getCountry());
    }

    public static AddressEntity toEntity(AddressDTO addressDTO) {
        if (addressDTO == null || addressDTO.id() == null) {
            return null;
        }
        AddressEntity address = new AddressEntity();
        address.setId(addressDTO.id());
        address.setStreet(addressDTO.street());
        address.setCity(addressDTO.city());
        address.setPostalCode(addressDTO.zipcode());
        address.setCountry(addressDTO.country());
        return address;
    }

    public static AddressEntity toEntity(Address addressDTO) {
        AddressEntity address = new AddressEntity();
        address.setId(addressDTO.getId());
        address.setStreet(addressDTO.getStreet());
        address.setCity(addressDTO.getCity());
        address.setPostalCode(addressDTO.getZipcode());
        address.setCountry(addressDTO.getCountry());
        return address;
    }

    public static Address toAddress(AddressDTO address) {
        if (address == null || address.id() == null) {
            return null;
        }
        Address addressDTO = new Address();
        addressDTO.setId(address.id());
        addressDTO.setStreet(address.street());
        addressDTO.setCity(address.city());
        addressDTO.setZipcode(address.zipcode());
        addressDTO.setCountry(address.country());
        return addressDTO;
    }
}
