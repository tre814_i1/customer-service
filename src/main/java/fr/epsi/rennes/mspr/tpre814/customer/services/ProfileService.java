package fr.epsi.rennes.mspr.tpre814.customer.services;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.UserProfileEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.ProfileRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProfileService {
    private final ProfileRepository repo;

    public UserProfileEntity save(UserProfileEntity profile) {
        if (profile == null) {
            log.warn("Profile is null");
            return null;
        }
        log.info("Saving profile: {}", profile);
        return repo.save(profile);
    }
}
