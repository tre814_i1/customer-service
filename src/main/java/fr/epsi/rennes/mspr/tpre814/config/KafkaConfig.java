package fr.epsi.rennes.mspr.tpre814.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

import java.util.concurrent.TimeUnit;


@EnableKafka
@Configuration
public class KafkaConfig {
    public static final long TIMEOUT = 10;
    public static final TimeUnit TIMEOUT_UNIT = TimeUnit.SECONDS;
    /**
     * Topics names definition
     */
    public static final String RESERVE_TOPIC = "stock-reserve";
    public static final String STOCK_RESPONSE = "stock-response";
    public static final String CUSTOMER_REQUEST = "customer-request";
    public static final String CUSTOMER_RESPONSE = "customer-response";
    public static final String ORDER_REQUEST = "request-orders";
    public static final String ORDER_RESPONSE = "orders-response";


    /**
     * Services ID definition
     */
    @Value("${spring.kafka.client-id}")
    public static final String SERVICE_KAFKA_GROUP = "${spring.kafka.client-id}";


}
