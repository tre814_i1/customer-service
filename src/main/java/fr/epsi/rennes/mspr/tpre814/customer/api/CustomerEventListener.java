package fr.epsi.rennes.mspr.tpre814.customer.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.customer.services.interfaces.CustomerService;
import fr.epsi.rennes.mspr.tpre814.customer.util.CustomerMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.CUSTOMER_REQUEST;
import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.CUSTOMER_RESPONSE;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerEventListener {
    private static final ObjectMapper mapper = new ObjectMapper();
    private final KafkaTemplate<UUID, String> anyTemplate;
    private final CustomerService service;

    private static Object getRequest(String req, Class<?> clazz) {
        log.debug("Parsing request");
        try {
            return mapper.readValue(req, clazz);
        } catch (Exception e) {
            throw new RuntimeException("Cannot parse request: " + req, e);
        }
    }

    //##############################################################################################################
    //################################################ LISTENERS ##################################################
    //##############################################################################################################


    @KafkaListener(topics = CUSTOMER_REQUEST, groupId = "fake-customer-service-1",
            autoStartup = "true", info = "Fake Customer Service Listener")
    public void listenCustomer(String req,
                               @Header(KafkaHeaders.RECEIVED_KEY) String key
    ) {
        UUID requestId = UUID.fromString(key);
        log.info("Key : {}", requestId);

        CustomerRequest request = (CustomerRequest) getRequest(req, CustomerRequest.class);
        request.getOrderId();
        CustomerResponse response = new CustomerResponse(requestId);
        CustomerEntity entity = service.getEntity(request.getId());
        service.handleCustomerOrdered(request.getId(), request.getOrderId());
        response.setPayload(CustomerMapper.toCustomer(CustomerMapper.toDTO(entity)));
        log.info("Response : {}", response.toJson());
        anyTemplate.send(CUSTOMER_RESPONSE, requestId, response.toJson());
    }
}