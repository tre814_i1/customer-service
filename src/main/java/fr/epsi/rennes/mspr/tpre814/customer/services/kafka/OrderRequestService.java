package fr.epsi.rennes.mspr.tpre814.customer.services.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderRequestService extends AwaitTable {
    private final KafkaTemplate<UUID, String> anyTemplate;


    @Async
    public CompletableFuture<BaseEvent> getOrders(UUID key, List<UUID> list) {
        log.info("Requesting orders {}", list);
        CompletableFuture<BaseEvent> fut = newRequest(key);
        log.debug("ORDERS JSON REQUEST :" + list.stream().map(UUID::toString).toList());
        anyTemplate.send(ORDER_REQUEST, key, list.stream().map(UUID::toString).toList().toString());
        return fut;
    }

    //##############################################################################################################
    //################################################ LISTENERS ##################################################
    //##############################################################################################################

    /**
     * Await for kafka responses
     *
     * @param response the Customer to listenOrderRequests, received from the customer service
     */
    @KafkaListener(topics = ORDER_RESPONSE, groupId = SERVICE_KAFKA_GROUP)
    public void listenOrders(@RequestBody String response,
                             @Header(KafkaHeaders.RECEIVED_KEY) UUID key) throws JsonProcessingException {
        log.info("Orders response received for {}", key);
        log.info("Received Orders => {}", response);
        CustomerResponse responseEvent;
        try {
            responseEvent = mapper.readValue(response, CustomerResponse.class);
        } catch (JsonProcessingException e) {
            try {
                responseEvent = mapper.readValue(response
                        .replaceAll("\\[\\{(.+)}]",
                                "{$1}"), CustomerResponse.class);
            } catch (JsonProcessingException ex) {
                log.error("Error parsing response", ex);
                return;
            }
        }
        getPending(responseEvent.getId()).complete(responseEvent);
    }
}