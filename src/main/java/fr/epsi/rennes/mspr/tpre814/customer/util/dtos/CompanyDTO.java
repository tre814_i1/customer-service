package fr.epsi.rennes.mspr.tpre814.customer.util.dtos;

public record CompanyDTO(String id, String name, String siret, AddressDTO address) {
}
