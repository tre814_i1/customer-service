package fr.epsi.rennes.mspr.tpre814.customer.util;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.shared.events.OrderResponseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class CustomerMapper {

    public static CustomerEntity toEntity(Customer customer) {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(customer.getId());
        customerEntity.setUsername(customer.getUsername());
        customerEntity.setProfile(ProfileMapper.toEntity(customer.getProfile()));
        customerEntity.setAddress(AddressMapper.toEntity(customer.getAddress()));
        customerEntity.setCompany(CompanyMapper.toEntity(customer.getCompany()));
        return customerEntity;
    }

    public static CustomerDTO toDTO(CustomerEntity customerEntity) {
        if (customerEntity == null)
            return null;
        return new CustomerDTO(
                customerEntity.getId().toString(),
                customerEntity.getUsername(),
                AddressMapper.toDTO(customerEntity.getAddress()),
                CompanyMapper.toDTO(customerEntity.getCompany()),
                ProfileMapper.toDTO(customerEntity.getProfile()),
                customerEntity.getOrders().stream()
                        .filter(Objects::nonNull)
                        .map(UUID::toString).toList()
        );
    }

    public static Customer toCustomer(CustomerDTO customerDTO) {
        Customer customer = new Customer();
        customer.setId(UUID.fromString(customerDTO.id()));
        customer.setUsername(customerDTO.username());
        customer.setProfile(ProfileMapper.toProfile(customerDTO.profile()));
        customer.setAddress(AddressMapper.toAddress(customerDTO.address()));
        customer.setCompany(CompanyMapper.toCompany(customerDTO.company()));
        return customer;
    }

    public static List<CustomerDTO> toDTO(List<CustomerEntity> all) {
        return all.stream().map(CustomerMapper::toDTO).toList();
    }

    public static void updateWith(CustomerEntity dbCustomer, CustomerEntity entity) {
        dbCustomer.setUsername(entity.getUsername());
        dbCustomer.setProfile(entity.getProfile());
        dbCustomer.setAddress(entity.getAddress());
        dbCustomer.setCompany(entity.getCompany());
        dbCustomer.setOrders(entity.getOrders());
    }

    public static CustomerDTO toDTO(CustomerEntity customer, OrderResponseEvent orders) {
        return new CustomerDTO(
                customer.getId().toString(),
                customer.getUsername(),
                AddressMapper.toDTO(customer.getAddress()),
                CompanyMapper.toDTO(customer.getCompany()),
                ProfileMapper.toDTO(customer.getProfile()),
                orders.getPayload().stream().map(OrderDTO::toJson).toList()
        );
    }
}
