package fr.epsi.rennes.mspr.tpre814.customer.util.dtos;

import java.util.List;

public record CustomerDTO(String id, String username, AddressDTO address, CompanyDTO company, UserProfileDTO profile, List<String> orders) {
}
