package fr.epsi.rennes.mspr.tpre814.customer.database.repository;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.AddressEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.UserProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProfileRepository extends JpaRepository<UserProfileEntity, UUID> {

}
