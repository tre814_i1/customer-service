package fr.epsi.rennes.mspr.tpre814.customer.util;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.AddressEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CompanyEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.UserProfileEntity;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.AddressDTO;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CompanyDTO;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.UserProfileDTO;
import fr.epsi.rennes.mspr.tpre814.shared.events.OrderResponseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.customer.services.CustomerServiceImpl.fakeCustomer;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CustomerMapperDiffblueTest {
    /**
     * Method under test: {@link CustomerMapper#toDTO(CustomerEntity)}
     */
    @Test
    void testToDTO() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customerEntity);

        // Assert
        UserProfileDTO profileResult = actualToDTOResult.profile();
        assertEquals("Doe", profileResult.lastName());
        CompanyDTO companyResult = actualToDTOResult.company();
        AddressDTO addressResult = companyResult.address();
        assertEquals("GB", addressResult.country());
        AddressDTO addressResult2 = actualToDTOResult.address();
        assertEquals("GB", addressResult2.country());
        assertEquals("Jane", profileResult.firstName());
        assertEquals("Name", companyResult.name());
        assertEquals("Oxford", addressResult.city());
        assertEquals("Oxford", addressResult2.city());
        assertEquals("Postal Code", addressResult.zipcode());
        assertEquals("Postal Code", addressResult2.zipcode());
        assertEquals("Siret", companyResult.siret());
        assertEquals("Street", addressResult.street());
        assertEquals("Street", addressResult2.street());
        assertEquals("janedoe", actualToDTOResult.username());
        assertEquals(2, addressResult.id().variant());
        assertEquals(2, addressResult2.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(CustomerEntity)}
     */
    @Test
    void testToDTO2() {
        // Arrange, Act and Assert
        assertNull(CustomerMapper.toDTO((CustomerEntity) null));
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(CustomerEntity)}
     */
    @Test
    void testToDTO3() {
        // Arrange
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(null);
        customerEntity.setProfile(null);
        customerEntity.setCompany(null);

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customerEntity);

        // Assert
        assertEquals("janedoe", actualToDTOResult.username());
        assertNull(actualToDTOResult.company());
        assertNull(actualToDTOResult.profile());
        AddressDTO addressResult = actualToDTOResult.address();
        assertNull(addressResult.city());
        assertNull(addressResult.country());
        assertNull(addressResult.street());
        assertNull(addressResult.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(CustomerEntity)}
     */
    @Test
    void testToDTO4() {
        // Arrange
        CompanyEntity company = new CompanyEntity();
        company.setName("Name");
        company.setSiret("Siret");
        company.setAddress(null);
        company.setId(null);

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(null);
        customerEntity.setProfile(null);
        customerEntity.setCompany(company);

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customerEntity);

        // Assert
        assertEquals("janedoe", actualToDTOResult.username());
        assertNull(actualToDTOResult.company());
        assertNull(actualToDTOResult.profile());
        AddressDTO addressResult = actualToDTOResult.address();
        assertNull(addressResult.city());
        assertNull(addressResult.country());
        assertNull(addressResult.street());
        assertNull(addressResult.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(CustomerEntity)}
     */
    @Test
    void testToDTO5() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setPostalCode("Postal Code");
        address.setStreet("Street");
        address.setId(null);

        CompanyEntity company = new CompanyEntity();
        company.setName("Name");
        company.setSiret("Siret");
        company.setAddress(address);
        company.setId(UUID.randomUUID());

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(null);
        customerEntity.setProfile(null);
        customerEntity.setCompany(company);

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customerEntity);

        // Assert
        CompanyDTO companyResult = actualToDTOResult.company();
        assertEquals("Name", companyResult.name());
        assertEquals("Siret", companyResult.siret());
        assertEquals("janedoe", actualToDTOResult.username());
        assertNull(actualToDTOResult.profile());
        AddressDTO addressResult = companyResult.address();
        assertNull(addressResult.city());
        AddressDTO addressResult2 = actualToDTOResult.address();
        assertNull(addressResult2.city());
        assertNull(addressResult.country());
        assertNull(addressResult2.country());
        assertNull(addressResult.street());
        assertNull(addressResult2.street());
        assertNull(addressResult.zipcode());
        assertNull(addressResult2.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertEquals(2, addressResult2.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test:
     * {@link CustomerMapper#toDTO(CustomerEntity, OrderResponseEvent)}
     */
    @Test
    void testToDTO6() {
        // Arrange
        CustomerEntity customer = new CustomerEntity();
        customer.setId(UUID.randomUUID());
        customer.setOrders(new ArrayList<>());
        customer.setUsername("janedoe");
        customer.setAddress(null);
        customer.setProfile(null);
        customer.setCompany(null);

        OrderResponseEvent orders = new OrderResponseEvent();
        orders.setPayload(new ArrayList<>());

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customer, orders);

        // Assert
        assertEquals("janedoe", actualToDTOResult.username());
        assertNull(actualToDTOResult.company());
        assertNull(actualToDTOResult.profile());
        AddressDTO addressResult = actualToDTOResult.address();
        assertNull(addressResult.city());
        assertNull(addressResult.country());
        assertNull(addressResult.street());
        assertNull(addressResult.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test:
     * {@link CustomerMapper#toDTO(CustomerEntity, OrderResponseEvent)}
     */
    @Test
    void testToDTO7() {
        // Arrange
        CompanyEntity company = new CompanyEntity();
        company.setName("Name");
        company.setSiret("Siret");
        company.setAddress(null);
        company.setId(null);

        CustomerEntity customer = new CustomerEntity();
        customer.setId(UUID.randomUUID());
        customer.setOrders(new ArrayList<>());
        customer.setUsername("janedoe");
        customer.setAddress(null);
        customer.setProfile(null);
        customer.setCompany(company);

        OrderResponseEvent orders = new OrderResponseEvent();
        orders.setPayload(new ArrayList<>());

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customer, orders);

        // Assert
        assertEquals("janedoe", actualToDTOResult.username());
        assertNull(actualToDTOResult.company());
        assertNull(actualToDTOResult.profile());
        AddressDTO addressResult = actualToDTOResult.address();
        assertNull(addressResult.city());
        assertNull(addressResult.country());
        assertNull(addressResult.street());
        assertNull(addressResult.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test:
     * {@link CustomerMapper#toDTO(CustomerEntity, OrderResponseEvent)}
     */
    @Test
    void testToDTO8() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setPostalCode("Postal Code");
        address.setStreet("Street");
        address.setId(null);

        CompanyEntity company = new CompanyEntity();
        company.setName("Name");
        company.setSiret("Siret");
        company.setAddress(address);
        company.setId(UUID.randomUUID());

        CustomerEntity customer = new CustomerEntity();
        customer.setId(UUID.randomUUID());
        customer.setOrders(new ArrayList<>());
        customer.setUsername("janedoe");
        customer.setAddress(null);
        customer.setProfile(null);
        customer.setCompany(company);

        OrderResponseEvent orders = new OrderResponseEvent();
        orders.setPayload(new ArrayList<>());

        // Act
        CustomerDTO actualToDTOResult = CustomerMapper.toDTO(customer, orders);

        // Assert
        CompanyDTO companyResult = actualToDTOResult.company();
        assertEquals("Name", companyResult.name());
        assertEquals("Siret", companyResult.siret());
        assertEquals("janedoe", actualToDTOResult.username());
        assertNull(actualToDTOResult.profile());
        AddressDTO addressResult = companyResult.address();
        assertNull(addressResult.city());
        AddressDTO addressResult2 = actualToDTOResult.address();
        assertNull(addressResult2.city());
        assertNull(addressResult.country());
        assertNull(addressResult2.country());
        assertNull(addressResult.street());
        assertNull(addressResult2.street());
        assertNull(addressResult.zipcode());
        assertNull(addressResult2.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertEquals(2, addressResult2.id().variant());
        assertTrue(actualToDTOResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO9() {
        // Arrange and Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(new ArrayList<>());

        // Assert
        assertTrue(actualToDTOResult.isEmpty());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO10() {
        // Arrange
        ArrayList<CustomerEntity> all = new ArrayList<>();
        all.add(null);

        // Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(all);

        // Assert
        assertEquals(1, actualToDTOResult.size());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO11() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        ArrayList<CustomerEntity> all = new ArrayList<>();
        all.add(customerEntity);

        // Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(all);

        // Assert
        assertEquals(1, actualToDTOResult.size());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO12() {
        // Arrange
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(null);
        customerEntity.setProfile(null);
        customerEntity.setCompany(null);

        ArrayList<CustomerEntity> all = new ArrayList<>();
        all.add(customerEntity);

        // Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(all);

        // Assert
        assertEquals(1, actualToDTOResult.size());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO13() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setPostalCode("Postal Code");
        address.setStreet("Street");
        address.setId(null);

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(address);
        customerEntity.setProfile(null);
        customerEntity.setCompany(null);

        ArrayList<CustomerEntity> all = new ArrayList<>();
        all.add(customerEntity);

        // Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(all);

        // Assert
        assertEquals(1, actualToDTOResult.size());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO14() {
        // Arrange
        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setLastName("Doe");
        profile.setId(null);

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(null);
        customerEntity.setProfile(profile);
        customerEntity.setCompany(null);

        ArrayList<CustomerEntity> all = new ArrayList<>();
        all.add(customerEntity);

        // Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(all);

        // Assert
        assertEquals(1, actualToDTOResult.size());
    }

    /**
     * Method under test: {@link CustomerMapper#toDTO(List)}
     */
    @Test
    void testToDTO15() {
        // Arrange
        CompanyEntity company = new CompanyEntity();
        company.setName("Name");
        company.setSiret("Siret");
        company.setAddress(null);
        company.setId(null);

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setUsername("janedoe");
        customerEntity.setAddress(null);
        customerEntity.setProfile(null);
        customerEntity.setCompany(company);

        ArrayList<CustomerEntity> all = new ArrayList<>();
        all.add(customerEntity);

        // Act
        List<CustomerDTO> actualToDTOResult = CustomerMapper.toDTO(all);

        // Assert
        assertEquals(1, actualToDTOResult.size());
    }

    /**
     * Method under test: {@link CustomerMapper#toCustomer(CustomerDTO)}
     */
    @Test
    void testToCustomer() {
        Customer customer = fakeCustomer(UUID.randomUUID());
        CustomerDTO customerDTO = CustomerMapper.toDTO(CustomerMapper.toEntity(customer));
        // Act
        Customer actualToCustomerResult = CustomerMapper.toCustomer(customerDTO);

        // Assert
        assertThat(actualToCustomerResult)
                .usingRecursiveComparison()
                .ignoringFields("createdAt", "updatedAt", "profile.createdAt", "profile.updatedAt", "address.createdAt", "address.updatedAt", "company.createdAt", "company.updatedAt")
                .isEqualTo(customer);
    }
}
