package fr.epsi.rennes.mspr.tpre814.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.sql.Connection;
import java.sql.SQLException;


@Testcontainers
@EnableKafka
@SpringBootApplication
public class IntegrationDummyApplication {
    // DOCKER CONTAINERS
    @Container
    public static final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:16")
            .withDatabaseName("integration")
            .withUsername("sa")
            .withPassword("sa");
    // KAFKA CONTAINERS
    @Container
    public static final KafkaContainer kafka = new KafkaContainer(
            DockerImageName.parse("confluentinc/cp-kafka:7.6.1")
    );

    public static void main(String[] args) {
        System.out.println("""
                #########################################################################################
                #########################################################################################
                        
                IntegrationDummyApplication.init
                        
                #########################################################################################
                #########################################################################################
                """);
        SpringApplication.run(IntegrationDummyApplication.class, args);
    }

    public static String getKafkaBootstrap() {
        return kafka.getBootstrapServers();
    }

    public static Connection getPostgresConnection() throws SQLException {
        return postgreSQLContainer.createConnection("");
    }

    public static void start() {
        postgreSQLContainer.start();
        kafka.start();
    }

    public static String getPostgresUrl() {
        String url = postgreSQLContainer.getJdbcUrl()
                .replace("jdbc:", "jdbc:tc:");
        return url.substring(0, url.indexOf("?"));
    }

    public static void stop() {
        postgreSQLContainer.stop();
        kafka.stop();
    }
}
