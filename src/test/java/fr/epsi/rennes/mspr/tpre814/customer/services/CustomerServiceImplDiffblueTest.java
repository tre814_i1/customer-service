package fr.epsi.rennes.mspr.tpre814.customer.services;

import fr.epsi.rennes.mspr.tpre814.customer.database.entity.AddressEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CompanyEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.CustomerEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.entity.UserProfileEntity;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.AddressRepository;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.CompanyRepository;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.CustomerRepository;
import fr.epsi.rennes.mspr.tpre814.customer.database.repository.ProfileRepository;
import fr.epsi.rennes.mspr.tpre814.customer.error.CustomerNotFoundException;
import fr.epsi.rennes.mspr.tpre814.customer.services.kafka.OrderRequestService;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.AddressDTO;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CompanyDTO;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.CustomerDTO;
import fr.epsi.rennes.mspr.tpre814.customer.util.dtos.UserProfileDTO;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.OrderResponseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.*;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@ExtendWith({SoftAssertionsExtension.class})
@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
class CustomerServiceImplDiffblueTest {
    /**
     * Method under test: {@link CustomerServiceImpl#fakeCustomer(UUID)}
     */
    private CustomerRepository customerRepository;
    private AddressService addressService;
    private CompanyService companyService;
    private ProfileService profileService;
    @MockBean
    private OrderRequestService orderRequestService;
    private CustomerServiceImpl customerService;

    @BeforeEach
    void setUp() {
        customerRepository = mock(CustomerRepository.class);
        orderRequestService = mock(OrderRequestService.class);
        addressService = new AddressService(mock(AddressRepository.class));
        companyService = new CompanyService(mock(CompanyRepository.class), addressService);
        profileService = new ProfileService(mock(ProfileRepository.class));
        customerService = new CustomerServiceImpl(customerRepository, addressService, companyService, profileService, orderRequestService);
    }

    @Test
    void testFakeCustomer() {
        // Arrange and Act
        Customer actualFakeCustomerResult = CustomerServiceImpl.fakeCustomer(UUID.randomUUID());

        // Assert
        Company company = actualFakeCustomerResult.getCompany();
        assertEquals("123456789 00001", company.getSiret());
        UserProfile profile = actualFakeCustomerResult.getProfile();
        assertEquals("Bob", profile.getFirstName());
        assertEquals("Doe", profile.getLastName());
        Address address = company.getAddress();
        assertEquals("EPSI", address.getCity());
        assertEquals("EPSI", address.getCountry());
        assertEquals("EPSI", address.getStreet());
        assertEquals("EPSI", address.getZipcode());
        assertEquals("EPSI", company.getName());
        Address address2 = actualFakeCustomerResult.getAddress();
        assertEquals("Faker", address2.getCity());
        assertEquals("Faker", address2.getCountry());
        assertEquals("Faker", address2.getStreet());
        assertEquals("Faker", address2.getZipcode());
        assertEquals("Faker", actualFakeCustomerResult.getUsername());
        assertEquals(2, address.getId().variant());
        assertEquals(2, address2.getId().variant());
        assertEquals(2, company.getId().variant());
        assertEquals(2, actualFakeCustomerResult.getId().variant());
        assertEquals(2, profile.getId().variant());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#create(CustomerEntity)}
     */
    @Test
    void testCreate() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findByUsername(Mockito.<String>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Doe");

        CustomerEntity entity = new CustomerEntity();
        entity.setAddress(address3);
        entity.setCompany(company2);
        entity.setId(UUID.randomUUID());
        entity.setOrders(new ArrayList<>());
        entity.setProfile(profile2);
        entity.setUsername("janedoe");

        // Act
        CustomerDTO actualCreateResult = customerService.create(entity);

        // Assert
        assertNull(actualCreateResult);
    }

    /**
     * Method under test: {@link CustomerServiceImpl#get(UUID)}
     */
    @Test
    void testGet() {
        // Arrange
        UUID id = UUID.randomUUID();
        when(customerRepository.findById(id)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(CustomerNotFoundException.class, () -> customerService.get(id));
    }

    @Test
    void testGet_ok() {
        // Arrange
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(UUID.randomUUID());
        orderDTO.setCustomerId(UUID.randomUUID());

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.getOrders().add(orderDTO.getId());
        customerEntity.setId(UUID.randomUUID());

        when(customerRepository.findById(customerEntity.getId())).thenReturn(Optional.of(customerEntity));

        CompletableFuture<BaseEvent> orders = CompletableFuture.completedFuture(new OrderResponseEvent(UUID.randomUUID(), List.of(orderDTO)));
        when(orderRequestService.getOrders(customerEntity.getId(), customerEntity.getOrders())).thenReturn(orders);
        // Act and Assert
        customerService.get(customerEntity.getId());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#all()}
     */
    @Test
    void testAll() {
        // Arrange
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findAll()).thenReturn(new ArrayList<>());
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        // Act
        List<CustomerDTO> actualAllResult = (new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService)).all();

        // Assert
        verify(repository).findAll();
        verify(producerFactory).transactionCapable();
        assertTrue(actualAllResult.isEmpty());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#all()}
     */
    @Test
    void testAll2() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Getting all customers...");
        address.setStreet("Getting all customers...");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Getting all customers...");
        address2.setStreet("Getting all customers...");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Getting all customers...");
        company.setSiret("Getting all customers...");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        customerEntityList.add(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findAll()).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        // Act
        List<CustomerDTO> actualAllResult = (new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService)).all();

        // Assert
        verify(repository).findAll();
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualAllResult.size());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#all()}
     */
    @Test
    void testAll3() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Getting all customers...");
        address.setStreet("Getting all customers...");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Getting all customers...");
        address2.setStreet("Getting all customers...");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Getting all customers...");
        company.setSiret("Getting all customers...");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        AddressEntity address3 = new AddressEntity();
        address3.setCity("London");
        address3.setCountry("GBR");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("London");
        address4.setCountry("GBR");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Getting all customers...");
        profile2.setFirstName("John");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Smith");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("Getting all customers...");

        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        customerEntityList.add(customerEntity2);
        customerEntityList.add(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findAll()).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        // Act
        List<CustomerDTO> actualAllResult = (new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService)).all();

        // Assert
        verify(repository).findAll();
        verify(producerFactory).transactionCapable();
        assertEquals(2, actualAllResult.size());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#all()}
     */
    @Test
    void testAll4() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(null);
        address.setPostalCode("Getting all customers...");
        address.setStreet("Getting all customers...");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Getting all customers...");
        address2.setStreet("Getting all customers...");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Getting all customers...");
        company.setSiret("Getting all customers...");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        customerEntityList.add(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findAll()).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        // Act
        List<CustomerDTO> actualAllResult = (new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService)).all();

        // Assert
        verify(repository).findAll();
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualAllResult.size());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#all()}
     */
    @Test
    void testAll5() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Getting all customers...");
        address.setStreet("Getting all customers...");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Getting all customers...");
        address2.setStreet("Getting all customers...");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(null);
        company.setName("Getting all customers...");
        company.setSiret("Getting all customers...");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        customerEntityList.add(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findAll()).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        // Act
        List<CustomerDTO> actualAllResult = (new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService)).all();

        // Assert
        verify(repository).findAll();
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualAllResult.size());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#all()}
     */
    @Test
    void testAll6() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Getting all customers...");
        address.setStreet("Getting all customers...");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Getting all customers...");
        address2.setStreet("Getting all customers...");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Getting all customers...");
        company.setSiret("Getting all customers...");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(null);
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");

        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        customerEntityList.add(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findAll()).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        // Act
        List<CustomerDTO> actualAllResult = (new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService)).all();

        // Assert
        verify(repository).findAll();
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualAllResult.size());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#update(CustomerEntity)}
     */
    @Test
    void testUpdate() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Doe");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("janedoe");
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.save(Mockito.<CustomerEntity>any())).thenReturn(customerEntity2);
        when(repository.findById(Mockito.<UUID>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        AddressEntity address5 = new AddressEntity();
        address5.setCity("Oxford");
        address5.setCountry("GB");
        address5.setId(UUID.randomUUID());
        address5.setPostalCode("Postal Code");
        address5.setStreet("Street");

        AddressEntity address6 = new AddressEntity();
        address6.setCity("Oxford");
        address6.setCountry("GB");
        address6.setId(UUID.randomUUID());
        address6.setPostalCode("Postal Code");
        address6.setStreet("Street");

        CompanyEntity company3 = new CompanyEntity();
        company3.setAddress(address6);
        company3.setId(UUID.randomUUID());
        company3.setName("Name");
        company3.setSiret("Siret");

        UserProfileEntity profile3 = new UserProfileEntity();
        profile3.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile3.setFirstName("Jane");
        profile3.setId(UUID.randomUUID());
        profile3.setLastName("Doe");

        CustomerEntity entity = new CustomerEntity();
        entity.setAddress(address5);
        entity.setCompany(company3);
        entity.setId(UUID.randomUUID());
        entity.setOrders(new ArrayList<>());
        entity.setProfile(profile3);
        entity.setUsername("janedoe");

        // Act
        CustomerDTO actualUpdateResult = customerServiceImpl.update(entity);

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(CustomerEntity.class));
        verify(producerFactory).transactionCapable();
        UserProfileDTO profileResult = actualUpdateResult.profile();
        assertEquals("Doe", profileResult.lastName());
        CompanyDTO companyResult = actualUpdateResult.company();
        AddressDTO addressResult = companyResult.address();
        assertEquals("GB", addressResult.country());
        AddressDTO addressResult2 = actualUpdateResult.address();
        assertEquals("GB", addressResult2.country());
        assertEquals("Jane", profileResult.firstName());
        assertEquals("Name", companyResult.name());
        assertEquals("Oxford", addressResult.city());
        assertEquals("Oxford", addressResult2.city());
        assertEquals("Postal Code", addressResult.zipcode());
        assertEquals("Postal Code", addressResult2.zipcode());
        assertEquals("Siret", companyResult.siret());
        assertEquals("Street", addressResult.street());
        assertEquals("Street", addressResult2.street());
        assertEquals("janedoe", actualUpdateResult.username());
        assertEquals(2, addressResult.id().variant());
        assertEquals(2, addressResult2.id().variant());
        assertTrue(actualUpdateResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#update(CustomerEntity)}
     */
    @Test
    void testUpdate2() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(null);
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Doe");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("janedoe");
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.save(Mockito.<CustomerEntity>any())).thenReturn(customerEntity2);
        when(repository.findById(Mockito.<UUID>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        AddressEntity address5 = new AddressEntity();
        address5.setCity("Oxford");
        address5.setCountry("GB");
        address5.setId(UUID.randomUUID());
        address5.setPostalCode("Postal Code");
        address5.setStreet("Street");

        AddressEntity address6 = new AddressEntity();
        address6.setCity("Oxford");
        address6.setCountry("GB");
        address6.setId(UUID.randomUUID());
        address6.setPostalCode("Postal Code");
        address6.setStreet("Street");

        CompanyEntity company3 = new CompanyEntity();
        company3.setAddress(address6);
        company3.setId(UUID.randomUUID());
        company3.setName("Name");
        company3.setSiret("Siret");

        UserProfileEntity profile3 = new UserProfileEntity();
        profile3.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile3.setFirstName("Jane");
        profile3.setId(UUID.randomUUID());
        profile3.setLastName("Doe");

        CustomerEntity entity = new CustomerEntity();
        entity.setAddress(address5);
        entity.setCompany(company3);
        entity.setId(UUID.randomUUID());
        entity.setOrders(new ArrayList<>());
        entity.setProfile(profile3);
        entity.setUsername("janedoe");

        // Act
        CustomerDTO actualUpdateResult = customerServiceImpl.update(entity);

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(CustomerEntity.class));
        verify(producerFactory).transactionCapable();
        UserProfileDTO profileResult = actualUpdateResult.profile();
        assertEquals("Doe", profileResult.lastName());
        CompanyDTO companyResult = actualUpdateResult.company();
        AddressDTO addressResult = companyResult.address();
        assertEquals("GB", addressResult.country());
        assertEquals("Jane", profileResult.firstName());
        assertEquals("Name", companyResult.name());
        assertEquals("Oxford", addressResult.city());
        assertEquals("Postal Code", addressResult.zipcode());
        assertEquals("Siret", companyResult.siret());
        assertEquals("Street", addressResult.street());
        assertEquals("janedoe", actualUpdateResult.username());
        AddressDTO addressResult2 = actualUpdateResult.address();
        assertNull(addressResult2.city());
        assertNull(addressResult2.country());
        assertNull(addressResult2.street());
        assertNull(addressResult2.zipcode());
        assertEquals(2, addressResult.id().variant());
        assertEquals(2, addressResult2.id().variant());
        assertTrue(actualUpdateResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#update(CustomerEntity)}
     */
    @Test
    void testUpdate3() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(null);
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Doe");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("janedoe");
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.save(Mockito.<CustomerEntity>any())).thenReturn(customerEntity2);
        when(repository.findById(Mockito.<UUID>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        AddressEntity address5 = new AddressEntity();
        address5.setCity("Oxford");
        address5.setCountry("GB");
        address5.setId(UUID.randomUUID());
        address5.setPostalCode("Postal Code");
        address5.setStreet("Street");

        AddressEntity address6 = new AddressEntity();
        address6.setCity("Oxford");
        address6.setCountry("GB");
        address6.setId(UUID.randomUUID());
        address6.setPostalCode("Postal Code");
        address6.setStreet("Street");

        CompanyEntity company3 = new CompanyEntity();
        company3.setAddress(address6);
        company3.setId(UUID.randomUUID());
        company3.setName("Name");
        company3.setSiret("Siret");

        UserProfileEntity profile3 = new UserProfileEntity();
        profile3.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile3.setFirstName("Jane");
        profile3.setId(UUID.randomUUID());
        profile3.setLastName("Doe");

        CustomerEntity entity = new CustomerEntity();
        entity.setAddress(address5);
        entity.setCompany(company3);
        entity.setId(UUID.randomUUID());
        entity.setOrders(new ArrayList<>());
        entity.setProfile(profile3);
        entity.setUsername("janedoe");

        // Act
        CustomerDTO actualUpdateResult = customerServiceImpl.update(entity);

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(CustomerEntity.class));
        verify(producerFactory).transactionCapable();
        UserProfileDTO profileResult = actualUpdateResult.profile();
        assertEquals("Doe", profileResult.lastName());
        AddressDTO addressResult = actualUpdateResult.address();
        assertEquals("GB", addressResult.country());
        assertEquals("Jane", profileResult.firstName());
        assertEquals("Oxford", addressResult.city());
        assertEquals("Postal Code", addressResult.zipcode());
        assertEquals("Street", addressResult.street());
        assertEquals("janedoe", actualUpdateResult.username());
        assertNull(actualUpdateResult.company());
        assertEquals(2, addressResult.id().variant());
        assertTrue(actualUpdateResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#update(CustomerEntity)}
     */
    @Test
    void testUpdate4() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(null);
        profile2.setLastName("Doe");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("janedoe");
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.save(Mockito.<CustomerEntity>any())).thenReturn(customerEntity2);
        when(repository.findById(Mockito.<UUID>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        AddressEntity address5 = new AddressEntity();
        address5.setCity("Oxford");
        address5.setCountry("GB");
        address5.setId(UUID.randomUUID());
        address5.setPostalCode("Postal Code");
        address5.setStreet("Street");

        AddressEntity address6 = new AddressEntity();
        address6.setCity("Oxford");
        address6.setCountry("GB");
        address6.setId(UUID.randomUUID());
        address6.setPostalCode("Postal Code");
        address6.setStreet("Street");

        CompanyEntity company3 = new CompanyEntity();
        company3.setAddress(address6);
        company3.setId(UUID.randomUUID());
        company3.setName("Name");
        company3.setSiret("Siret");

        UserProfileEntity profile3 = new UserProfileEntity();
        profile3.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile3.setFirstName("Jane");
        profile3.setId(UUID.randomUUID());
        profile3.setLastName("Doe");

        CustomerEntity entity = new CustomerEntity();
        entity.setAddress(address5);
        entity.setCompany(company3);
        entity.setId(UUID.randomUUID());
        entity.setOrders(new ArrayList<>());
        entity.setProfile(profile3);
        entity.setUsername("janedoe");

        // Act
        CustomerDTO actualUpdateResult = customerServiceImpl.update(entity);

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(CustomerEntity.class));
        verify(producerFactory).transactionCapable();
        CompanyDTO companyResult = actualUpdateResult.company();
        AddressDTO addressResult = companyResult.address();
        assertEquals("GB", addressResult.country());
        AddressDTO addressResult2 = actualUpdateResult.address();
        assertEquals("GB", addressResult2.country());
        assertEquals("Name", companyResult.name());
        assertEquals("Oxford", addressResult.city());
        assertEquals("Oxford", addressResult2.city());
        assertEquals("Postal Code", addressResult.zipcode());
        assertEquals("Postal Code", addressResult2.zipcode());
        assertEquals("Siret", companyResult.siret());
        assertEquals("Street", addressResult.street());
        assertEquals("Street", addressResult2.street());
        assertEquals("janedoe", actualUpdateResult.username());
        assertNull(actualUpdateResult.profile());
        assertEquals(2, addressResult.id().variant());
        assertEquals(2, addressResult2.id().variant());
        assertTrue(actualUpdateResult.orders().isEmpty());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete() {
        // Arrange
        CustomerRepository repository = mock(CustomerRepository.class);
        doNothing().when(repository).deleteById(Mockito.<UUID>any());
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        // Act
        customerServiceImpl.delete(UUID.randomUUID());

        // Assert
        verify(repository).deleteById(isA(UUID.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link CustomerServiceImpl#createAll(List)}
     */
    @Test
    void testCreateAll() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        CustomerRepository repository = mock(CustomerRepository.class);
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        // Act
        List<CustomerDTO> actualCreateAllResult = customerServiceImpl.createAll(new ArrayList<>());

        // Assert
        verify(producerFactory).transactionCapable();
        assertTrue(actualCreateAllResult.isEmpty());
    }

    /**
     * Method under test: {@link CustomerServiceImpl#createAll(List)}
     */
    @Test
    void testCreateAll2() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.findByUsername(Mockito.<String>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Doe");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("janedoe");

        ArrayList<CustomerEntity> customer = new ArrayList<>();
        customer.add(customerEntity2);

        // Act
        List<CustomerDTO> actualCreateAllResult = customerServiceImpl.createAll(customer);

        // Assert
        verify(repository).findByUsername(eq("janedoe"));
        verify(producerFactory).transactionCapable();
        assertEquals(1, actualCreateAllResult.size());
    }

    /**
     * Method under test:
     * {@link CustomerServiceImpl#handleCustomerOrdered(UUID, UUID)}
     */
    @Test
    void testHandleCustomerOrdered() {
        // Arrange
        AddressEntity address = new AddressEntity();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setPostalCode("Postal Code");
        address.setStreet("Street");

        AddressEntity address2 = new AddressEntity();
        address2.setCity("Oxford");
        address2.setCountry("GB");
        address2.setId(UUID.randomUUID());
        address2.setPostalCode("Postal Code");
        address2.setStreet("Street");

        CompanyEntity company = new CompanyEntity();
        company.setAddress(address2);
        company.setId(UUID.randomUUID());
        company.setName("Name");
        company.setSiret("Siret");

        UserProfileEntity profile = new UserProfileEntity();
        profile.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile.setFirstName("Jane");
        profile.setId(UUID.randomUUID());
        profile.setLastName("Doe");

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setAddress(address);
        customerEntity.setCompany(company);
        customerEntity.setId(UUID.randomUUID());
        customerEntity.setOrders(new ArrayList<>());
        customerEntity.setProfile(profile);
        customerEntity.setUsername("janedoe");
        Optional<CustomerEntity> ofResult = Optional.of(customerEntity);

        AddressEntity address3 = new AddressEntity();
        address3.setCity("Oxford");
        address3.setCountry("GB");
        address3.setId(UUID.randomUUID());
        address3.setPostalCode("Postal Code");
        address3.setStreet("Street");

        AddressEntity address4 = new AddressEntity();
        address4.setCity("Oxford");
        address4.setCountry("GB");
        address4.setId(UUID.randomUUID());
        address4.setPostalCode("Postal Code");
        address4.setStreet("Street");

        CompanyEntity company2 = new CompanyEntity();
        company2.setAddress(address4);
        company2.setId(UUID.randomUUID());
        company2.setName("Name");
        company2.setSiret("Siret");

        UserProfileEntity profile2 = new UserProfileEntity();
        profile2.setCreatedAt("Jan 1, 2020 8:00am GMT+0100");
        profile2.setFirstName("Jane");
        profile2.setId(UUID.randomUUID());
        profile2.setLastName("Doe");

        CustomerEntity customerEntity2 = new CustomerEntity();
        customerEntity2.setAddress(address3);
        customerEntity2.setCompany(company2);
        customerEntity2.setId(UUID.randomUUID());
        customerEntity2.setOrders(new ArrayList<>());
        customerEntity2.setProfile(profile2);
        customerEntity2.setUsername("janedoe");
        CustomerRepository repository = mock(CustomerRepository.class);
        when(repository.save(Mockito.<CustomerEntity>any())).thenReturn(customerEntity2);
        when(repository.findById(Mockito.<UUID>any())).thenReturn(ofResult);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);
        UUID customerId = UUID.randomUUID();

        // Act
        customerServiceImpl.handleCustomerOrdered(customerId, UUID.randomUUID());

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(CustomerEntity.class));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link CustomerServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById() {
        // Arrange
        CustomerRepository repository = mock(CustomerRepository.class);
        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        // Act
        List<CustomerEntity> actualFindAllByIdResult = customerServiceImpl.findAllById(new ArrayList<>());

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).transactionCapable();
        assertTrue(actualFindAllByIdResult.isEmpty());
        assertSame(customerEntityList, actualFindAllByIdResult);
    }

    /**
     * Method under test: {@link CustomerServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById2() {
        // Arrange
        CustomerRepository repository = mock(CustomerRepository.class);
        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        ArrayList<UUID> list = new ArrayList<>();
        list.add(UUID.randomUUID());

        // Act
        List<CustomerEntity> actualFindAllByIdResult = customerServiceImpl.findAllById(list);

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).transactionCapable();
        assertTrue(actualFindAllByIdResult.isEmpty());
        assertSame(customerEntityList, actualFindAllByIdResult);
    }

    /**
     * Method under test: {@link CustomerServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById3() {
        // Arrange
        CustomerRepository repository = mock(CustomerRepository.class);
        ArrayList<CustomerEntity> customerEntityList = new ArrayList<>();
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(customerEntityList);
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));
        AddressService addrS = new AddressService(mock(AddressRepository.class));
        CompanyRepository repo = mock(CompanyRepository.class);
        CompanyService compS = new CompanyService(repo, new AddressService(mock(AddressRepository.class)));

        CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl(repository, addrS, compS, new ProfileService(mock(ProfileRepository.class)), orderRequestService);

        ArrayList<UUID> list = new ArrayList<>();
        list.add(UUID.randomUUID());
        list.add(UUID.randomUUID());

        // Act
        List<CustomerEntity> actualFindAllByIdResult = customerServiceImpl.findAllById(list);

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).transactionCapable();
        assertTrue(actualFindAllByIdResult.isEmpty());
        assertSame(customerEntityList, actualFindAllByIdResult);
    }
}
