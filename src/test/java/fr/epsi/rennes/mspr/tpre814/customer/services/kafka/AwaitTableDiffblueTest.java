package fr.epsi.rennes.mspr.tpre814.customer.services.kafka;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

class AwaitTableDiffblueTest {
    /**
     * Method under test: {@link AwaitTable#tearDown()}
     */
    @Test
    void testTearDown() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);

        // Act
        (new OrderRequestService(new KafkaTemplate<>(producerFactory))).tearDown();

        // Assert that nothing has changed
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link AwaitTable#newRequest(UUID)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testNewRequest() {
        // Arrange
        // TODO: Populate arranged inputs
        AwaitTable awaitTable = null;
        UUID id = null;

        // Act
        CompletableFuture<BaseEvent> actualNewRequestResult = awaitTable.newRequest(id);

        // Assert
        // TODO: Add assertions on result
    }

    /**
     * Method under test: {@link AwaitTable#getPending(UUID)}
     */
    @Test
    void testGetPending() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        when(producerFactory.transactionCapable()).thenReturn(true);
        OrderRequestService orderRequestService = new OrderRequestService(new KafkaTemplate<>(producerFactory));

        // Act and Assert
        assertThrows(IllegalStateException.class, () -> orderRequestService.getPending(UUID.randomUUID()));
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test: {@link AwaitTable#getResponse(UUID)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testGetResponse() {
        // Arrange
        // TODO: Populate arranged inputs
        AwaitTable awaitTable = null;
        UUID id = null;

        // Act
        BaseEvent actualResponse = awaitTable.getResponse(id);

        // Assert
        // TODO: Add assertions on result
    }
}
