package fr.epsi.rennes.mspr.tpre814.customer.services.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.events.OrderResponseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class OrderRequestServiceDiffblueTest {
    /**
     * Method under test: {@link OrderRequestService#getOrders(UUID, List)}
     */
    @Test
    void testGetOrders() {
        // Arrange
        KafkaTemplate<UUID, String> kafkaTemplate = mock(KafkaTemplate.class);
        OrderRequestService orderRequestService = new OrderRequestService(kafkaTemplate);
        UUID key = UUID.randomUUID();
        List<UUID> list = List.of(UUID.randomUUID());

        // Act
        CompletableFuture<BaseEvent> actualOrders = orderRequestService.getOrders(key, list);

        // Assert
        assertThat(actualOrders).isNotNull();
        verify(kafkaTemplate).send(isA(String.class), isA(UUID.class), isA(String.class));
    }

    /**
     * Method under test: {@link OrderRequestService#listenOrders(String, UUID)}
     */
    @Test
    void testListenOrders() throws JsonProcessingException {
        // Arrange
        OrderRequestService orderRequestService = new OrderRequestService(mock(KafkaTemplate.class));
        OrderDTO responseOrder = new OrderDTO();
        responseOrder.setCreatedAt(null);
        responseOrder.setCustomerId(UUID.randomUUID());
        responseOrder.setId(UUID.randomUUID());
        responseOrder.setStatus(null);
        responseOrder.setTotalPrice(0.0);
        OrderResponseEvent response = new OrderResponseEvent(UUID.randomUUID(), List.of(responseOrder));
        UUID key = UUID.randomUUID();

        // Act
        orderRequestService.listenOrders(response.toJson(), key);

        // Assert
        assertThat(orderRequestService.pendingRequests).isNotNull();
    }
}
