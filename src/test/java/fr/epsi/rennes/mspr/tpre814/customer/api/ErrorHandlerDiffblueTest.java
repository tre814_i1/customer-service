package fr.epsi.rennes.mspr.tpre814.customer.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.epsi.rennes.mspr.tpre814.customer.error.CustomerError;
import fr.epsi.rennes.mspr.tpre814.customer.error.CustomerNotFoundException;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.listener.KafkaBackoffException;

class ErrorHandlerDiffblueTest {
    /**
     * Method under test:
     * {@link ErrorHandler#handleOrderNotFoundException(CustomerNotFoundException)}
     */
    @Test
    void testHandleOrderNotFoundException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleOrderNotFoundExceptionResult = errorHandler
                .handleOrderNotFoundException(new CustomerNotFoundException());

        // Assert
        assertNull(actualHandleOrderNotFoundExceptionResult.getBody());
        assertEquals(404, actualHandleOrderNotFoundExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleOrderNotFoundExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleOrderValidationError(CustomerError)}
     */
    @Test
    void testHandleOrderValidationError() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleOrderValidationErrorResult = errorHandler
                .handleOrderValidationError(new CustomerError("Msg"));

        // Assert
        assertEquals("Msg", actualHandleOrderValidationErrorResult.getBody());
        assertEquals(400, actualHandleOrderValidationErrorResult.getStatusCodeValue());
        assertTrue(actualHandleOrderValidationErrorResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleEnumConstantNotPresentException(EnumConstantNotPresentException)}
     */
    @Test
    void testHandleEnumConstantNotPresentException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();
        Class<Enum> forNameResult = Enum.class;

        // Act
        ResponseEntity<String> actualHandleEnumConstantNotPresentExceptionResult = errorHandler
                .handleEnumConstantNotPresentException(new EnumConstantNotPresentException(forNameResult, "foo"));

        // Assert
        assertEquals("Invalid state : foo", actualHandleEnumConstantNotPresentExceptionResult.getBody());
        assertEquals(400, actualHandleEnumConstantNotPresentExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleEnumConstantNotPresentExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test: {@link ErrorHandler#handleException(Exception)}
     */
    @Test
    void testHandleException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleExceptionResult = errorHandler.handleException(new Exception("foo"));

        // Assert
        assertEquals("Internal server error", actualHandleExceptionResult.getBody());
        assertEquals(500, actualHandleExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleKafkaBackoffException(KafkaBackoffException)}
     */
    @Test
    void testHandleKafkaBackoffException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleKafkaBackoffExceptionResult = errorHandler.handleKafkaBackoffException(
                new KafkaBackoffException("An error occurred", new TopicPartition("Topic", 1), "42", 1L));

        // Assert
        assertEquals("Service Unavailable An error occurred", actualHandleKafkaBackoffExceptionResult.getBody());
        assertEquals(503, actualHandleKafkaBackoffExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleKafkaBackoffExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleException(Exception)} (NullPointerException)}
     */
    @Test
    void testHandleNullPointerException() {
        // Arrange
        ErrorHandler errorHandler = new ErrorHandler();

        // Act
        ResponseEntity<String> actualHandleNullPointerExceptionResult = errorHandler
                .handleException(new NullPointerException("foo"));

        // Assert
        assertEquals("Internal server error", actualHandleNullPointerExceptionResult.getBody());
        assertEquals(500, actualHandleNullPointerExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleNullPointerExceptionResult.getHeaders().isEmpty());
    }
}
