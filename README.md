# Multi-Service Application PaieTonKawa : Customer Service
Auteurs :
- Mistayan - [Gitlab](https://gitlab.com/Mistayan/)

Contributeurs :
- Bob-117 - [Gitlab](https://gitlab.com/Bob-117/)
- Sullfurick - [Gitlab](https://gitlab.com/Sullfurick)


Développé avec :
- Java 21+
- SpringBoot 3.2.5+
- Kafka 2.2.7+
- Gradle 8.7+
- PostgreSQL 16.3+
- Docker latest

____

[TOC]

____

## Customer Service
   
### Description

Ce projet est un exemple d'application multi-services utilisant Spring Boot et Spring Cloud. Il est composé de 3 services :
- order-service : Service de gestion des commandes
- product-service : Service de gestion des produits
- customer-service : Service de gestion des clients


### Architecture

```mermaid
graph v
    subgraph middlewares
        I[API Gateway] <--> S[Keycloak]
    end
    subgraph frontend
        K[Frontend]
        K <--> I
    end
    subgraph micro-services
        A[Customer Service]
        B[Order Service]
        C[Product Service]
        I <--> A[Customer Service]
        I <--> B[Order Service]
        I <--> C[Product Service]
        subgraph data
            D[Postgres]
            E[Postgres]
            F[Postgres]
        end
        subgraph broker
            Y[Kafka Broker] <--> Z[Zookeeper]
        end
        D <--> A <--> Y 
        E <--> B <--> Y
        F <--> C <--> Y
    end
```

### Installation

1. Cloner le projet
    ```shell
    git clone https://gitlab.com/tre814_i1/customer-service.git
    ```
   
2. Se rendre dans le dossier du projet
    ```shell
    cd customer-service
    ```
   
3. Lancer la commande `./gradlew bootBuildImage` pour construire le programme en Image:
   cette étape est optionnelle, les images existent déjà sur un repository en ligne, PUBLIC 

   
4. Créer un fichier .env a vous:
    ```shell
    cp .env.example .env
    nano .env
    ```
   
5. Puis `docker compose up -f ... -d` pour lancer les compose requis
    ```shell
    docker compose -f compose.kafka.yal up -d
    ```
   attendez quelques secondes, puis 
    ```shell
    docker compose -f compose.app_customer.yml up -d
    ```

### Endpoints

- GET /customers : Récupérer la liste des clients
- POST /customers : Créer un client
- GET /customers/{id} : Récupérer un client
- PUT /customers/{id} : Mettre à jour un client
- DELETE /customers/{id} : Supprimer un client

## Fonctionnement de l'application

#### Collection Postman
- [Collection Postman](https://api.postman.com/collections/24975391-32fbb431-8db9-4e0b-bc81-92dfab6498cc?access_key=PMAT-01J0DNVA4R7F761V4C9N9XCJ6R)
#### Uses Cases
- Créer un client
```mermaid
sequenceDiagram
    Frontend ->> API Gateway: POST /customers
    alt authenticated ? 
    API Gateway ->> Frontend: 401 Unauthorized
    else
    API Gateway ->> Customer Service: POST /customers
        Customer Service ->> Postgres: INSERT INTO customers
    end
    Postgres ->> Customer Service: Customer saved
    Customer Service ->> API Gateway: 200 Created
    API Gateway ->> Frontend: 200 Created

```
- Récupérer un client
```mermaid
sequenceDiagram
    Frontend ->> API Gateway: GET /customers/{id}
    alt authenticated ? 
    API Gateway ->> Frontend: 401 Unauthorized
    else
    API Gateway ->> Customer Service: GET /customers/{id}
    end
    Customer Service ->> Postgres: findCustomerById({id})
    Postgres ->> Customer Service: Customer        
    # if customer got orders, request order-service
    alt customer.orders.length > 0
    Customer Service -->> Broker: OrdersRequest(customer.id, customer.orders)
    Broker -->> Order Service: OrdersRequest(customer.id, customer.orders)
    alt timeout
        Order Service -->> Broker: OrdersResponse([])
    else
        Order Service -->> Broker: OrdersResponse(orders)
    end
        Broker ->> Customer Service: OrdersResponse(orders)
    end
    Customer Service ->> API Gateway: 200 OK [Customer]
    API Gateway ->> Frontend: 200 OK [Customer]
```
- Mettre à jour un client
- Supprimer un client

- Récupérer la liste des clients
____

#### Créer un client

- URL : `http://localhost:30050/customers`
- Méthode : POST
- Body :

```json
{
  "username": "Bobbie_Dickens",
  "address": {
    "postalCode": "48348",
    "city": "East Toniton"
  },
  "profile": {
    "firstName": "Bradford",
    "lastName": "Kuphal"
  },
  "company": {
    "companyName": "Fritsch, Bayer and Sanford"
  }
}
```

      - Optionnel à la création du compte: `company`, `address`
      - Obligatoires pour commander : `username`, `profile`, `address`

- Réponse :

```json
{
  "id": 13,
  "name": "Bridget Torp"
}
```

#### Récupérer un client

- URL : `http://localhost:30050/customers/{id}`
- Méthode : GET
- Paramètre : id
- Exemple : `http://localhost:30050/customers/13`
- Réponse :

```json
{
  "id": "13",
  "createdAt": "2023-08-30T13:26:04.774Z",
  "username": "Bobbie_Dickens",
  "address": {
    "postalCode": "48348",
    "city": "East Toniton"
  },
  "profile": {
    "firstName": "Bradford",
    "lastName": "Kuphal"
  },
  "company": {
    "companyName": "Fritsch, Bayer and Sanford"
  },
  "orders": [
    {
      "createdAt": "2023-08-29T21:22:06.346Z",
      "id": "5",
      "customerId": "13"
    }
  ]
}
```

...

